# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'jma/version'

Gem::Specification.new do |spec|
  spec.name          = 'jma'
  spec.version       = JMA::VERSION
  spec.authors       = ['smapira']
  spec.email         = ['info@routeflags.com']

  spec.summary       = 'UNOFFICIAL Japan Meteorological Agency (気象庁) API client'
  spec.description   = 'This repository contains a gem for the Japan Meteorological Agency (気象庁) for searching new information simplifies.'
  spec.homepage      = 'https://bitbucket.org/routeflagsinc/jma/'
  spec.license       = 'MIT'

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'activesupport'
  spec.add_dependency 'faraday'
  spec.add_dependency 'faraday-http-cache'
  spec.add_dependency 'faraday_middleware'
  spec.add_dependency 'multi_xml'
  spec.add_dependency 'rss'
  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rubocop'
end
