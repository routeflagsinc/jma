# frozen_string_literal: true

require 'spec_helper'
require 'jma/client'
require 'json'

RSpec.describe 'amedas_selector_information', vcr: { cassette_name: 'amedas_selector_information' } do
  let(:client) do
    JMA::Client.new.tap { |config| config.response_type = :hash }
  end

  let(:response) { client.request_amedas_selector_information.body }
  it do
    expect(response).to be_an Array
  end

  it do
    expect(response.size).to eq 1
  end

  it do
    expect(response[0].keys).to eq %w[key values]
  end

  it do
    expect(response[0]['key']).to eq 'elem'
  end

  it do
    expect(response[0]['values'][0].keys).to eq %w[name enName value]
  end

  it do
    expect(response[0]['values'][0]['name']).to eq '10分間降水量'
  end

  it do
    expect(response[0]['values'][0]['enName']).to eq 'Precipitation (10-min.)'
  end

  it do
    expect(response[0]['values'][0]['value']).to eq 'precipitation10m'
  end
end
