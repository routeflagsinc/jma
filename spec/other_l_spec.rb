# frozen_string_literal: true

require 'spec_helper'
require 'jma/client'
require 'json'

RSpec.describe 'other_long', vcr: { cassette_name: 'other_long' } do
  let(:client) do
    JMA::Client.new.tap { |config| config.response_type = :hash }
  end

  let(:response) { client.request_other_long }

  it do
    expect(response.items.is_a?(Array)).to be_truthy
  end

  it do
    expect(response.items.map { |x| x.title.content }).to include '地方海上警報（Ｈ２８）'
  end

  it do
    expect(response.items.map do |x|
             x.id.content
           end).to include 'http://www.data.jma.go.jp/developer/xml/data/20211229143932_0_VPCU51_460100.xml'
  end

  it do
    expect(response.items.map { |x| x.updated.content.to_s }).to include '2021-12-29T14:39:30Z'
  end

  it do
    expect(response.items.map { |x| x.content.content }.sort.uniq).to include '【全般海上警報】'
  end
end
