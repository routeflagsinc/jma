# frozen_string_literal: true

require 'spec_helper'
require 'jma/client'
require 'json'

RSpec.describe 'jmatile_n2', vcr: { cassette_name: 'jmatile_n2' } do
  let(:client) do
    JMA::Client.new.tap { |config| config.response_type = :hash }
  end

  let(:response) { client.request_jmatile_n2.body }
  it do
    expect(response).to be_an Array
  end

  it do
    expect(response.size).to eq 12
  end

  it do
    expect(response[0].keys).to eq %w[basetime validtime elements]
  end

  it do
    expect(response[0]['basetime']).to eq '20220103072000'
  end

  it do
    expect(response[0]['elements']).to eq %w[hrpns hrpns_nd]
  end
end
