# frozen_string_literal: true

require 'spec_helper'
require 'jma/client'
require 'json'

RSpec.describe JMA do
  it 'has a version number' do
    expect(JMA::VERSION).not_to be nil
  end
end
