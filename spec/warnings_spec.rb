# frozen_string_literal: true

require 'spec_helper'
require 'jma/client'
require 'json'
require 'rss'

RSpec.describe 'warning', vcr: { cassette_name: 'warning' } do
  let(:client) do
    JMA::Client.new.tap { |config| config.response_type = :hash }
  end

  let(:response) { client.request_warning(130_000).body }
  it do
    expect(response).to be_an Hash
  end

  it do
    expect(response.keys).to eq %w[reportDatetime publishingOffice headlineText notice areaTypes timeSeries]
  end

  it do
    expect(response['publishingOffice']).to eq '気象庁'
  end

  it do
    expect(response['headlineText']).to eq  '伊豆諸島北部、伊豆諸島南部では、強風や高波に注意してください。東京地方では、空気の乾燥した状態が続くため、火の取り扱いに注意してください。'
  end

  it do
    expect(response['notice']).to eq '令和３年１０月７日の千葉県北西部の地震で揺れの大きかった足立区では、大雨注意報の土壌雨量指数基準を通常より引き下げた暫定基準で運用しています。'
  end

  it do
    expect(response['areaTypes']).to be_an Array
  end

  it do
    expect(response['areaTypes'][0].keys).to eq ['areas']
  end

  it do
    expect(response['areaTypes'][0]['areas'][0].keys).to eq %w[code warnings]
  end

  it do
    expect(response['areaTypes'][0]['areas'][0]['code']).to eq '130010' # city code
  end

  it do
    expect(response['areaTypes'][0]['areas'][0]['warnings'][0].keys).to eq %w[code status]
  end

  it do
    statuses = response['areaTypes'].map do |area_type|
      area_type['areas'].map do |area|
        area['warnings'].map do |x|
          x['status']
        end
      end
    end
                                    .flatten
                                    .sort
                                    .uniq
    expect(statuses).to eq %w[発表 発表警報・注意報はなし 継続]
  end

  it do
    information = JMA::Configurable::WARNING_INFORMATION[response['areaTypes'][0]['areas'][0]['warnings'][0]['code']]
    expect(information).to eq '乾燥注意報'
  end

  it do
    expect(response['timeSeries']).to be_an Array
  end

  it do
    expect(response['timeSeries'][0].keys).to eq %w[timeDefines areaTypes]
  end
end
