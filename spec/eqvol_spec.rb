# frozen_string_literal: true

require 'spec_helper'
require 'jma/client'
require 'json'

RSpec.describe 'eqvol', vcr: { cassette_name: 'eqvol' } do
  let(:client) do
    JMA::Client.new.tap { |config| config.response_type = :hash }
  end

  let(:response) { client.request_eqvol }

  it do
    expect(response.items.is_a?(Array)).to be_truthy
  end

  it do
    expect(response.items.map do |x|
             x.title.content
           end.sort.uniq).to eq %w[噴火に関する火山観測報 火山の状況に関する解説情報 降灰予報（定時） 震源・震度に関する情報]
  end

  it do
    expect(response.items.map do |x|
             x.id.content
           end).to include 'http://www.data.jma.go.jp/developer/xml/data/20220102221133_0_VXSE53_010000.xml'
  end

  it do
    expect(response.items.map { |x| x.updated.content.to_s }).to include '2022-01-03T06:28:24Z'
  end
end
