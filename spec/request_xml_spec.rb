# frozen_string_literal: true

require 'spec_helper'
require 'jma/client'
require 'json'

RSpec.describe 'request_xml', vcr: { cassette_name: 'request_xml' } do
  let(:client) do
    JMA::Client.new.tap { |config| config.response_type = :hash }
  end

  let(:response) do
    client.request_xml('http://www.data.jma.go.jp/developer/xml/data/20211229170106_0_VFVO52_400000.xml').body
  end

  it do
    expect(response.is_a?(Hash)).to be_truthy
  end

  it do
    expect(response.keys).to eq ['Report']
  end

  it do
    expect(response['Report'].keys).to eq %w[xmlns xmlns:jmx Control Head Body]
  end

  it do
    expect(response['Report']['Control'].keys).to eq %w[Title DateTime Status EditorialOffice PublishingOffice]
  end

  it do
    expect(response['Report']['Control']['Title']).to eq '噴火に関する火山観測報'
  end

  it do
    expect(response['Report']['Control']['DateTime']).to eq '2021-12-29T17:01:05Z'
  end

  it do
    expect(response['Report']['Control']['Status']).to eq '通常'
  end

  it do
    expect(response['Report']['Control']['EditorialOffice']).to eq '福岡管区気象台'
  end

  it do
    expect(response['Report']['Head'].keys).to eq %w[xmlns Title ReportDateTime TargetDateTime EventID InfoType Serial
                                                     InfoKind InfoKindVersion Headline]
  end

  it do
    expect(response['Report']['Head']['Title']).to eq '火山名　諏訪之瀬島　噴火に関する火山観測報'
  end

  it do
    expect(response['Report']['Head']['Status']).to eq nil
  end

  it do
    expect(response['Report']['Body'].keys).to eq %w[xmlns xmlns:jmx_eb VolcanoInfo VolcanoObservation]
  end

  it do
    expect(response['Report']['Body']['VolcanoInfo'].keys).to eq %w[type Item]
  end

  it do
    expect(response['Report']['Body']['VolcanoInfo']['type']).to eq '噴火に関する火山観測報'
  end

  it do
    expect(response['Report']['Body']['VolcanoInfo']['Item'].keys).to eq %w[EventTime Kind Areas]
  end

  it do
    expect(response['Report']['Body']['VolcanoObservation'].keys).to eq %w[ColorPlume OtherObservation]
  end

  it do
    expect(response['Report']['Body']['VolcanoObservation']['ColorPlume'].keys).to eq %w[PlumeHeightAboveCrater
                                                                                         PlumeHeightAboveSeaLevel PlumeDirection]
  end

  it do
    expect(response['Report']['Body']['VolcanoObservation']['OtherObservation']).to be_truthy
  end
end
