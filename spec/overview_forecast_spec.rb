# frozen_string_literal: true

require 'spec_helper'
require 'jma/client'
require 'json'

RSpec.describe 'overview_forecast', vcr: { cassette_name: 'overview_forecast' } do
  let(:client) do
    JMA::Client.new.tap { |config| config.response_type = :hash }
  end

  let(:response) { client.request_overview_forecast(130_000).body }
  it do
    expect(response).to be_an Hash
  end

  it do
    expect(response.keys).to eq %w[publishingOffice reportDatetime targetArea headlineText text]
  end

  it do
    expect(response['publishingOffice']).to eq '気象庁'
  end

  it do
    expect(response['reportDatetime']).to eq '2022-01-03T10:35:00+09:00'
  end

  it do
    expect(response['targetArea']).to eq '東京都'
  end

  it do
    expect(response['headlineText']).to eq ''
  end
end
