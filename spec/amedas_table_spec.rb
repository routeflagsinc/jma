# frozen_string_literal: true

require 'spec_helper'
require 'jma/client'
require 'json'

RSpec.describe 'amedas_table', vcr: { cassette_name: 'amedas_table' } do
  let(:client) do
    JMA::Client.new.tap { |config| config.response_type = :hash }
  end

  let(:response) { client.request_amedas_table.body }
  it do
    expect(response).to be_an Hash
  end

  it do
    expect(response.keys.size).to eq 1286
  end

  it do
    expect(response.keys).to include '11001'
  end

  it do
    expect(response['11001'].keys).to eq %w[type elems lat lon alt kjName knName enName]
  end

  it do
    expect(response['11001']['type']).to eq 'C'
  end

  it do
    expect(response['11001']['elems']).to eq '11112010'
  end

  it do
    expect(response['11001']['lat']).to eq [45, 31.2]
  end

  it do
    expect(response['11001']['lon']).to eq [141, 56.1]
  end

  it do
    expect(response['11001']['alt']).to eq 26
  end

  it do
    expect(response['11001']['kjName']).to eq '宗谷岬'
  end

  it do
    expect(response['11001']['knName']).to eq 'ソウヤミサキ'
  end

  it do
    expect(response['11001']['enName']).to eq 'Cape Soya'
  end
end
