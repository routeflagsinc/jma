# frozen_string_literal: true

require 'spec_helper'
require 'jma/client'
require 'json'

RSpec.describe 'himawari_fd', vcr: { cassette_name: 'himawari_fd' } do
  let(:client) do
    JMA::Client.new.tap { |config| config.response_type = :hash }
  end
  let(:response) { client.request_himawari_fd.body }
  it do
    expect(response).to be_an Array
  end

  it do
    expect(response[0].keys).to eq %w[basetime validtime]
  end

  it do
    expect(response[0]['basetime']).to eq '20220101193000'
  end
end
