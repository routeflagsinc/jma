# frozen_string_literal: true

require 'spec_helper'
require 'jma/client'
require 'json'

RSpec.describe 'extra_long', vcr: { cassette_name: 'extra_long' } do
  let(:client) do
    JMA::Client.new.tap { |config| config.response_type = :hash }
  end

  let(:response) { client.request_extra_long }

  it do
    expect(response.items.is_a?(Array)).to be_truthy
  end

  it do
    expect(response.items.map { |x| x.title.content }).to include '気象警報・注意報（Ｈ２７）'
  end

  it do
    expect(response.items.map do |x|
             x.id.content
           end).to include 'http://www.data.jma.go.jp/developer/xml/data/20211229165913_0_VPWW54_150000.xml'
  end

  it do
    expect(response.items.map { |x| x.updated.content.to_s }).to include '2021-12-29T16:59:11Z'
  end

  it do
    expect(response.items.map { |x| x.content.content }.sort.uniq).to include '【三重県気象警報・注意報】三重県では、３０日朝から強風に注意してください。'
  end
end
