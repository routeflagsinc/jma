# frozen_string_literal: true

require 'spec_helper'
require 'jma/client'
require 'json'

RSpec.describe 'regular_long', vcr: { cassette_name: 'regular_long' } do
  let(:client) do
    JMA::Client.new.tap { |config| config.response_type = :hash }
  end

  let(:response) { client.request_regular_long }

  it do
    expect(response.items.is_a?(Array)).to be_truthy
  end

  it do
    expect(response.items.map { |x| x.title.content }).to include 'アジア太平洋地上実況図'
  end

  it do
    expect(response.items.map do |x|
             x.id.content
           end).to include 'http://www.data.jma.go.jp/developer/xml/data/20211229023030_0_VPRN50_010000.xml'
  end

  it do
    expect(response.items.map { |x| x.updated.content.to_s }).to include '2021-12-29T02:30:01Z'
  end

  it do
    expect(response.items.map { |x| x.content.content }.sort.uniq).to include '【アジア太平洋地上実況図】'
  end
end
