# frozen_string_literal: true

require 'spec_helper'
require 'jma/client'
require 'json'

RSpec.describe 'amedas_map', vcr: { cassette_name: 'amedas_map' } do
  let(:client) do
    JMA::Client.new.tap { |config| config.response_type = :hash }
  end

  let(:response) { client.request_amedas_map(20_211_230_020_000).body }
  it do
    expect(response).to be_an Hash
  end

  it do
    expect(response.keys.size).to eq 1286
  end

  it do
    expect(response.keys).to include '11001'
  end

  it do
    expect(response['11001'].keys).to eq %w[temp humidity snow1h snow6h snow12h snow24h sun10m sun1h precipitation10m
                                            precipitation1h precipitation3h precipitation24h windDirection wind]
  end

  it do
    expect(response['11001']['temp']).to eq [-2.9, 0]
  end

  it do
    expect(response['11001']['humidity']).to eq [92, 0]
  end

  it do
    expect(response['11001']['snow1h']).to eq [0, nil]
  end

  it do
    expect(response['11001']['snow6h']).to eq [0, nil]
  end

  it do
    expect(response['11001']['snow12h']).to eq [0, nil]
  end

  it do
    expect(response['11001']['snow24h']).to eq [0, nil]
  end

  it do
    expect(response['11001']['sun10m']).to eq [0, 0]
  end

  it do
    expect(response['11001']['sun1h']).to eq [0.0, 0]
  end

  it do
    expect(response['11001']['precipitation10m']).to eq [0.0, 0]
  end

  it do
    expect(response['11001']['precipitation1h']).to eq [0.0, 0]
  end

  it do
    expect(response['11001']['precipitation3h']).to eq [0.0, 0]
  end

  it do
    expect(response['11001']['precipitation24h']).to eq [1.5, 0]
  end

  it do
    expect(response['11001']['windDirection']).to eq [14, 0]
  end

  it do
    expect(response['11001']['wind']).to eq [9.9, 0]
  end
end
