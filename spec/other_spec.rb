# frozen_string_literal: true

require 'spec_helper'
require 'jma/client'
require 'json'

RSpec.describe 'other', vcr: { cassette_name: 'other' } do
  let(:client) do
    JMA::Client.new.tap { |config| config.response_type = :hash }
  end

  let(:response) { client.request_other }

  it do
    expect(response.items.is_a?(Array)).to be_truthy
  end

  it do
    expect(response.items.map { |x| x.title.content }).to include '地方海上警報（Ｈ２８）'
  end

  it do
    expect(response.items.map do |x|
             x.id.content
           end).to include 'http://www.data.jma.go.jp/developer/xml/data/20220102202731_0_VPZU50_010000.xml'
  end

  it do
    expect(response.items.map { |x| x.updated.content.to_s }).to include '2022-01-03T05:35:19Z'
  end

  it do
    expect(response.items.map { |x| x.content.content }.sort.uniq).to include '【全般海上警報】'
  end
end
