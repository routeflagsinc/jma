# frozen_string_literal: true

require 'spec_helper'
require 'jma/client'
require 'json'

RSpec.describe 'eqvol_long', vcr: { cassette_name: 'eqvol_long' } do
  let(:client) do
    JMA::Client.new.tap { |config| config.response_type = :hash }
  end

  let(:response) { client.request_eqvol_long }

  it do
    expect(response.items.is_a?(Array)).to be_truthy
  end

  it do
    expect(response.items.map { |x| x.title.content }.sort.uniq).to include '噴火に関する火山観測報'
  end

  it do
    expect(response.items.map do |x|
             x.id.content
           end).to include 'http://www.data.jma.go.jp/developer/xml/data/20211229170106_0_VFVO52_400000.xml'
  end

  it do
    expect(response.items.map { |x| x.updated.content.to_s }).to include '2021-12-29T17:01:05Z'
  end
end
