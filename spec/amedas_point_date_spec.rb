# frozen_string_literal: true

require 'spec_helper'
require 'jma/client'
require 'json'

RSpec.describe 'amedas_point_date', vcr: { cassette_name: 'amedas_point_date' } do
  let(:client) do
    JMA::Client.new.tap { |config| config.response_type = :hash }
  end

  let(:response) { client.request_amedas_point_date(11_001, 20_211_228, '00').body }
  it do
    expect(response).to be_an Hash
  end

  it do
    expect(response.keys.size).to eq 18
  end

  it do
    expect(response.keys).to include '20211228000000'
  end

  it do
    expect(response['20211228000000'].keys).to eq %w[prefNumber observationNumber temp humidity snow1h snow6h snow12h
                                                     snow24h sun10m sun1h precipitation10m precipitation1h precipitation3h precipitation24h windDirection wind maxTempTime maxTemp minTempTime minTemp gustTime gustDirection gust]
  end

  it do
    expect(response['20211228000000']['prefNumber']).to eq 11
  end

  it do
    expect(response['20211228000000']['observationNumber']).to eq 1
  end

  it do
    expect(response['20211228000000']['temp']).to eq  [-6.2, 0]
  end

  it do
    expect(response['20211228000000']['humidity']).to eq [82, 0]
  end

  it do
    expect(response['20211228000000']['snow1h']).to eq [0, nil]
  end

  it do
    expect(response['20211228000000']['sun10m']).to eq [0, 0]
  end

  it do
    expect(response['20211228000000']['precipitation10m']).to eq [0.0, 0]
  end

  it do
    expect(response['20211228000000']['windDirection']).to eq [16, 0]
  end

  it do
    expect(response['20211228000000']['wind']).to eq [9.2, 0]
  end

  it do
    expect(response['20211228000000']['maxTempTime']['hour']).to eq 1
  end

  it do
    expect(response['20211228000000']['maxTemp']).to eq [-3.7, 0]
  end

  it do
    expect(response['20211228000000']['maxTempTime']['hour']).to eq 1
  end

  it do
    expect(response['20211228000000']['gustTime']['hour']).to eq 16
  end

  it do
    expect(response['20211228000000']['gustDirection']).to eq [14, 0]
  end

  it do
    expect(response['20211228000000']['gust']).to eq [22.9, 0]
  end
end
