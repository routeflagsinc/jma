# frozen_string_literal: true

require 'spec_helper'
require 'jma/client'
require 'json'

RSpec.describe 'extra', vcr: { cassette_name: 'extra' } do
  let(:client) do
    JMA::Client.new.tap { |config| config.response_type = :hash }
  end

  let(:response) { client.request_extra }

  it do
    expect(response.items.is_a?(Array)).to be_truthy
  end

  it do
    expect(response.items.map { |x| x.title.content }).to include '気象警報・注意報（Ｈ２７）'
  end

  it do
    expect(response.items.map do |x|
             x.id.content
           end).to include 'http://www.data.jma.go.jp/developer/xml/data/20220102212929_0_VPWW53_016000.xml'
  end

  it do
    expect(response.items.map { |x| x.content.content }.sort.uniq).to include '【京都府気象警報・注意報】京都府では、３日までなだれに注意してください。'
  end
end
