# frozen_string_literal: true

require 'spec_helper'
require 'jma/client'
require 'json'

RSpec.describe 'area_codes', vcr: { cassette_name: 'area_codes' } do
  let(:client) do
    JMA::Client.new.tap { |config| config.response_type = :hash }
  end

  let(:response) { client.request_area_codes.body }
  it do
    expect(response).to be_an Hash
  end

  it do
    expect(response.keys.size).to eq 5
  end

  it do
    expect(response.keys).to eq %w[centers offices class10s class15s class20s]
  end

  it do
    expect(response['centers'].keys).to eq %w[010100 010200 010300 010400 010500 010600 010700 010800 010900 011000
                                              011100]
  end

  it do
    expect(response['centers']['010100'].keys).to eq %w[name enName officeName children]
  end

  it do
    expect(response['centers']['010100']['name']).to eq '北海道地方'
  end
end
