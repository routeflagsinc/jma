# frozen_string_literal: true

require 'spec_helper'
require 'jma/client'
require 'json'

RSpec.describe 'amedas_latest_time', vcr: { cassette_name: 'amedas_latest_time' } do
  let(:client) do
    JMA::Client.new.tap { |config| config.response_type = :hash }
  end

  let(:response) { client.request_amedas_latest_time.body }
  it do
    expect(response).to be_an String
  end

  it do
    expect(response).to eq '2022-01-03T16:10:00+09:00'
  end
end
