# frozen_string_literal: true

require 'spec_helper'
require 'jma/client'
require 'json'

RSpec.describe 'forecast', vcr: { cassette_name: 'forecast' } do
  let(:client) do
    JMA::Client.new.tap { |config| config.response_type = :hash }
  end

  let(:response) { client.request_forecast(130_000).body }
  it do
    expect(response).to be_an Array
  end

  it do
    expect(response[0].keys).to eq %w[publishingOffice reportDatetime timeSeries]
  end

  it do
    expect(response[0]['publishingOffice']).to eq '気象庁'
  end
end
