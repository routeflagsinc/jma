# frozen_string_literal: true

require 'jma/configurable'
require 'jma/error'
require 'jma/raise_error'
require 'faraday'
require 'faraday-http-cache'
require 'faraday_middleware'
require 'net/http'
require 'rss'

module JMA
  class Client
    include JMA::Configurable

    ENDPOINTS.merge(FEED_ENDPOINTS).each do |method_name, path|
      define_method method_name do |*arguments|
        if ENDPOINTS.keys.include? method_name
          request_api(path, *arguments)
        else
          request_feed path
        end
      end
    end

    # The root path that Faraday is being loaded from.
    #
    # This is the root from where the libraries are auto-loaded.
    #
    # @return [Faraday::Response]
    # @param url [String]
    def request_xml(url)
      connection = build_connection url
      connection.headers = connection_options[:headers]
      connection.get url
    end

    private

    def switch_parser(path)
      return nil if response_type == RESPONSE_TYPE_RAW || path == 'bosai/amedas/data/latest_time.txt'
      return :json if ENDPOINTS.values.include? path

      :xml
    end

    def build_connection(path)
      type = switch_parser(path)
      Faraday.new do |f|
        f.use Faraday::HttpCache, store: cache, serializer: Marshal
        f.use JMA::Response::RaiseError
        f.response :follow_redirects
        f.response :logger, logger
        f.response type unless type.nil?
        f.request :retry
        f.adapter Faraday.default_adapter
      end
    end

    def build_api_endpoint(path, arguments)
      if arguments.size == 3
        return [API_BASE_URL,
                path, arguments[0], "#{arguments[1]}_#{arguments[2]}.json"].join('/')
      end
      return [API_BASE_URL, path, "#{arguments.first}.json"].join('/') if arguments.size == 1

      [API_BASE_URL, path].join('/')
    end

    def request_api(path, *arguments)
      endpoint = build_api_endpoint path, arguments
      connection = build_connection path
      connection.headers = connection_options[:headers]
      connection.get endpoint
    end

    def fetch_atom_feed(endpoint)
      client = Net::HTTP.new(endpoint.hostname, endpoint.port)
      client.use_ssl = true
      client.verify_mode = OpenSSL::SSL::VERIFY_NONE
      client.set_debug_output logger
      last_modified_at = cache.read endpoint.path.split(%r{/}).join('_').to_sym
      request = Net::HTTP::Get.new(endpoint.path,
                                   { 'If-Modified-Since' => last_modified_at }
                                      .merge(connection_options[:headers]))
      client.request request
    end

    def request_feed(path)
      endpoint = URI("#{FEED_BASE_URL}/#{path}")
      response = fetch_atom_feed endpoint
      error = Error.from_response response
      raise error if error

      if response.is_a?(Net::HTTPSuccess)
        cache.write(endpoint.path.split(%r{/}).join('_').to_sym,
                    response['last-modified'])
      end

      if response_type == RESPONSE_TYPE_RAW
        response
      elsif response.is_a?(Net::HTTPSuccess)
        RSS::Parser.parse(response.body)
      end
    end
  end
end
