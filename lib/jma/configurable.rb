# frozen_string_literal: true

require 'logger'
require 'active_support'

module JMA
  module Configurable
    attr_writer :response_type, :logger, :temporary_directory, :cache

    API_BASE_URL = 'https://www.jma.go.jp'
    FEED_BASE_URL = 'https://www.data.jma.go.jp'

    ENDPOINTS = {
      request_area_codes: 'bosai/common/const/area.json',
      request_amedas_table: 'bosai/amedas/const/amedastable.json',
      request_amedas_selector_information: 'bosai/const/selectorinfos/amedas.json',
      request_forecast: 'bosai/forecast/data/forecast',
      request_overview_forecast: 'bosai/forecast/data/overview_forecast',
      request_amedas_map: 'bosai/amedas/data/map',
      request_amedas_point: 'bosai/amedas/data/point',
      request_amedas_point_date: 'bosai/amedas/data/point',
      request_amedas_latest_time: 'bosai/amedas/data/latest_time.txt',
      request_jmatile_n1: 'bosai/jmatile/data/nowc/targetTimes_N1.json',
      request_jmatile_n2: 'bosai/jmatile/data/nowc/targetTimes_N2.json',
      request_himawari_jp: 'bosai/himawari/data/satimg/targetTimes_jp.json',
      request_himawari_fd: 'bosai/himawari/data/satimg/targetTimes_fd.json',
      request_warning: 'bosai/warning/data/warning'
    }.freeze

    FEED_ENDPOINTS = {
      request_regular: 'developer/xml/feed/regular.xml',
      request_extra: 'developer/xml/feed/extra.xml',
      request_eqvol: 'developer/xml/feed/eqvol.xml',
      request_other: 'developer/xml/feed/other.xml',
      request_regular_long: 'developer/xml/feed/regular_l.xml',
      request_extra_long: 'developer/xml/feed/extra_l.xml',
      request_eqvol_long: 'developer/xml/feed/eqvol_l.xml',
      request_other_long: 'developer/xml/feed/other_l.xml'
    }.freeze

    USER_AGENT = "JMA Ruby Gem #{JMA::VERSION}".freeze

    TEMPORARY = 'tmp'

    RESPONSE_TYPE_RAW = :raw

    WARNING_INFORMATION = {
      '02' => '暴風雪警報',
      '03' => '大雨警報',
      '04' => '洪水警報',
      '05' => '暴風警報',
      '06' => '大雪警報',
      '07' => '波浪警報',
      '08' => '高潮警報',
      '10' => '大雨注意報',
      '12' => '大雪警報',
      '13' => '風雪注意報',
      '14' => '雷注意報',
      '15' => '強風注意報',
      '16' => '波浪注意報',
      '17' => '融雪注意報',
      '18' => '洪水注意報',
      '19' => '高潮注意報',
      '20' => '濃霧注意報',
      '21' => '乾燥注意報',
      '22' => 'なだれ注意報',
      '23' => '低温注意報',
      '24' => '霜注意報',
      '25' => '着氷注意報',
      '26' => '着雪注意報',
      '32' => '暴風雪特別警報',
      '33' => '大雨特別警報',
      '35' => '暴風特別警報',
      '36' => '大雪特別警報',
      '37' => '波浪特別警報'
    }.freeze

    # Default options for Faraday::Connection
    # @return [Hash]
    def connection_options
      { headers: { user_agent: user_agent } }
    end

    # Default User-Agent header string from ENV or {USER_AGENT}
    # @return [String]
    def user_agent
      ENV['JMA_USER_AGENT'] || USER_AGENT
    end

    # Default Logger object from argument or Logger.new(STDOUT)
    # @return [String]
    def logger
      @logger || Logger.new($stdout)
    end

    # Default path to temporary directory location from argument or {TEMPORARY}
    # @return [String]
    def temporary_directory
      @temporary_directory || TEMPORARY
    end

    # Default path to temporary directory location from argument or {TEMPORARY}
    # @return [String]
    def response_type
      @response_type || RESPONSE_TYPE_RAW
    end

    def cache
      @cache || ActiveSupport::Cache.lookup_store(:file_store, temporary_directory)
    end
  end
end
