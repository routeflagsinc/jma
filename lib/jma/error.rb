# frozen_string_literal: true

module JMA
  # Custom error class for rescuing from all API errors
  # Inspired by https://github.com/octokit/octokit.rb
  class Error < StandardError
    # Returns the appropriate JMA::Error subclass based
    # on status and response message
    #
    # @param [Net::HTTPResponse] response HTTP response
    # @return [JMA::Error]
    def self.from_response(response)
      if response.methods.include? :code
        return unless (klass = match_status response.code.to_i, response.body.to_s)
      else
        return unless (klass = match_status response[:status].to_i,
                                            response[:body].to_s)

      end
      klass.new(response)
    end

    def self.match_status(status, body)
      case status
      when 400
        JMA::BadRequest
      when 401
        error_for_401
      when 403
        error_for_403 body
      when 404
        error_for_404 body
      when 405
        JMA::MethodNotAllowed
      when 406
        JMA::NotAcceptable
      when 409
        JMA::Conflict
      when 400..499
        match_status_410_500 status
      when 500..599
        match_status_over_500 status
      end
    end

    def self.match_status_410_500(status)
      case status
      when 415
        JMA::UnsupportedMediaType
      when 422
        JMA::UnprocessableEntity
      when 451
        JMA::UnavailableForLegalReasons
      when 400..499
        JMA::ClientError
      else
        JMA::ClientError
      end
    end

    def self.match_status_over_500(status)
      case status
      when 500
        JMA::InternalServerError
      when 501
        JMA::NotImplemented
      when 502
        JMA::BadGateway
      when 503
        JMA::ServiceUnavailable
      when 500..599
        JMA::ServerError
      else
        JMA::ServerError
      end
    end

    def initialize(response = nil)
      @response = response
      super(build_error_message)
    end

    # Returns most appropriate error for 401 HTTP status code
    # @private
    def self.error_for_401
      JMA::Unauthorized
    end

    # Returns most appropriate error for 403 HTTP status code
    # @private
    def self.error_for_403(body)
      case body
      when /rate limit exceeded/i
        JMA::TooManyRequests
      when /login attempts exceeded/i
        JMA::TooManyLoginAttempts
      when /abuse/i
        JMA::AbuseDetected
      else
        JMA::Forbidden
      end
    end

    # Return most appropriate error for 404 HTTP status code
    # @private
    def self.error_for_404
      JMA::NotFound
    end

    def build_error_message
      return nil if @response.nil?

      if @response.methods.include? :code
        "#{@response.code} - #{@response.message}"
      else
        message = "#{@response[:method].to_s.upcase} ".dup
        message << "#{@response[:url]}: "
        message << (@response[:status]).to_s
        message
      end
    end
  end

  # Raised on errors in the 400-499 range
  class ClientError < Error
  end

  # Raised when API returns a 400 HTTP status code
  class BadRequest < ClientError
  end

  # Raised when API returns a 401 HTTP status code
  class Unauthorized < ClientError
  end

  # Raised when API returns a 403 HTTP status code
  class Forbidden < ClientError
  end

  # Raised when API returns a 403 HTTP status code
  # and body matches 'rate limit exceeded'
  class TooManyRequests < Forbidden
  end

  # Raised when API returns a 403 HTTP status code
  # and body matches 'login attempts exceeded'
  class TooManyLoginAttempts < Forbidden
  end

  # Raised when API returns a 403 HTTP status code
  # and body matches 'abuse'
  class AbuseDetected < Forbidden
  end

  # Raised when API returns a 403 HTTP status code
  # and body matches 'repository access blocked'
  class RepositoryUnavailable < Forbidden
  end

  # Raised when API returns a 403 HTTP status code
  # and body matches 'email address must be verified'
  class UnverifiedEmail < Forbidden
  end

  # Raised when API returns a 403 HTTP status code
  # and body matches 'account was suspended'
  class AccountSuspended < Forbidden
  end

  # Raised when API returns a 404 HTTP status code
  class NotFound < ClientError
  end

  # Raised when API returns a 404 HTTP status code
  # and body matches 'Branch not protected'
  class BranchNotProtected < ClientError
  end

  # Raised when API returns a 405 HTTP status code
  class MethodNotAllowed < ClientError
  end

  # Raised when API returns a 406 HTTP status code
  class NotAcceptable < ClientError
  end

  # Raised when API returns a 409 HTTP status code
  class Conflict < ClientError
  end

  # Raised when API returns a 414 HTTP status code
  class UnsupportedMediaType < ClientError
  end

  # Raised when API returns a 422 HTTP status code
  class UnprocessableEntity < ClientError
  end

  # Raised when API returns a 451 HTTP status code
  class UnavailableForLegalReasons < ClientError
  end

  # Raised on errors in the 500-599 range
  class ServerError < Error
  end

  # Raised when API returns a 500 HTTP status code
  class InternalServerError < ServerError
  end

  # Raised when API returns a 501 HTTP status code
  class NotImplemented < ServerError
  end

  # Raised when API returns a 502 HTTP status code
  class BadGateway < ServerError
  end

  # Raised when API returns a 503 HTTP status code
  class ServiceUnavailable < ServerError
  end

  # Raised when client fails to provide valid Content-Type
  class MissingContentType < ArgumentError
  end

  # Raised when a method requires an application client_id
  # and secret but none is provided
  class ApplicationCredentialsRequired < StandardError
  end

  # Raised when a repository is created with an invalid format
  class InvalidRepository < ArgumentError
  end
end
