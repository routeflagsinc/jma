# frozen_string_literal: true

require 'faraday'

module JMA
  # Faraday response middleware
  module Response
    # This class raises an API-flavored exception based
    # HTTP status codes returned by the API
    class RaiseError < Faraday::Response::Middleware
      def on_complete(response)
        return unless (error = JMA::Error.from_response(response))

        raise error
      end
    end
  end
end
