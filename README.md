![Logo](https://blog.routeflags.com/wp-content/uploads/2022/02/jma.jpg)

# JMA - UNOFFICIAL Japan Meteorological Agency (気象庁) API client

![Ruby](https://img.shields.io/badge/Ruby-CC342D?style=for-the-badge&logo=ruby&logoColor=white)
[![Gem Version](https://badge.fury.io/rb/jma.svg)](https://badge.fury.io/rb/jma)
![](https://ruby-gem-downloads-badge.herokuapp.com/jma)
[![Ruby Style Guide](https://img.shields.io/badge/code_style-rubocop-brightgreen.svg)](https://github.com/rubocop-hq/rubocop)

This repository contains a gem for the Japan Meteorological Agency (気象庁) for searching new information simplifies. To use the API contains information, see the terms and conditions. ([気象庁ホームページについて](https://www.jma.go.jp/jma/kishou/info/coment.html))

## Sreenshots
### Rss
![RSS](https://blog.routeflags.com/wp-content/uploads/2022/02/rss_x6.gif)

### Api
![API](https://blog.routeflags.com/wp-content/uploads/2022/02/api_x6.gif)

## Features

- Overall retrieves information from the three APIs, Feed, Response XML and Response JSON.
- Favor requests a server with `If-Modified-Since`.
- Appreciatively displays the entire connection log.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'jma'
```

And then execute:

```shell
bundle install
```

Or install it yourself as:

```shell
gem install jma
```

## Configuration

To construct a client, available to configure a :response_type, :logger, :temporary_directory and :cache.

```ruby
client = JMA::Client.new.tap do |config|
  config.response_type = :raw
  config.logger = Logger.new($stdout)
  config.cache = ActiveSupport::Cache.lookup_store(:file_store, 'tmp')
end
```

### Constructor Details

Options Hash:

* :response_type(Symbol) -- The API response type, by default: response_type does not convert the response. You may specify below options..
    - :raw
    - :object
* :logger(Instance) -- The instance to logging, a default :logger will print log to display.
* :temporary_directory(String) -- The locate directory to store the cache, a default :temporary_directory is 'tmp' directory at run a gem.
* :cache(Instance) -- The cache instance that stores the time of the reply date from the API, a default value: cache will use `ActiveSupport::Cache`.

## Usage

### QQTW (Quickest Quick-start in The West)

```ruby
feed = JMA::Client.new.tap do |config|
	config.response_type = :deserialize
end.request_extra

puts <<~EOF
発表時刻: #{feed.updated.content.strftime('%Y-%m-%d %H:%M:%S')} 「#{feed.title.content} - #{feed.subtitle.content}」
#{feed.items.first.content.content} 
#{feed.items.first.id.content} 
#{feed.items.first.author.name.content} 
EOF

#=> 発表時刻: 2022-01-07 19:53:07 「高頻度（随時） - JMAXML publishing feed」
#=>  【石狩・空知・後志地方気象警報・注意報】後志地方では、８日までなだれに注意してください。 
#=>  http://www.data.jma.go.jp/developer/xml/data/20220107105234_0_VPWW53_016000.xml 
#=>  札幌管区気象台
```

### Client Operations

#### Feeds

When you specify `:deserialize` at `:response_type`, Obtain an `RSS::Atom::Entry` object.

#### Regular (定時)

気象に関する情報のうち、天気概況など定時に発表されるもの。毎分更新し、直近少なくとも10分入電を掲載しています。*1

* `#request_regular()`
    * High frequency (高頻度)
* `#request_regular_long()`
    * Long term (長期)

#### Extra (随時)

気象に関する情報のうち、警報・注意報など随時発表されるもの。毎分更新し、直近少なくとも10分入電を掲載しています。*1

* `#request_extra()`
    * High frequency (高頻度)
* `#request_extra_long()`
    * Long term (長期)

#### Earthquake and volcano (地震火山)

地震、火山に関する情報。毎分更新し、直近少なくとも10分入電を掲載しています。*1

* `request_eqvol()`
    * High frequency (高頻度)
* `request_eqvol_long()`
    * Long term (長期)

#### Other (その他)

その他の情報。毎分更新し、直近少なくとも10分入電を掲載しています。*1

* `request_other()`
    * High frequency (高頻度)
* `request_other_long()`
    * Long term (長期)

#### APIs

When you specify `:deserialize` at `:response_type`, Obtain a `Faraday::Response` object.

- `#request_area_codes()`
    - エリア一覧
- `#request_amedas_latest_time()`
    - アメダス 最新時刻
- `#request_amedas_map()`
    - アメダス（全国地図表示用）
- `#request_amedas_point()`
    - アメダス（地点系）
- `#request_amedas_point_date()`
    - アメダス（地点系）
- `#request_amedas_table()`
    - アメダス観測所一覧
- `#request_amedas_selector_information()`
    - アメダス配信データ 各要素の説明
- `#request_overview_forecast(n)`
    - 天気概況(当日/翌日)
- `#request_forecast(n)`
    - 天気予報
- `#request_warning()`
    - エリア毎の注意報/警報
- `#request_himawari_fd()`
    - ひまわり（フルディスク画像）の配信時刻
- `#request_himawari_jp()`
    - ひまわり（日本付近画像）の配信時刻
- `#request_jmatile_n1()`
    - レーダー実況の配信時刻
- `#request_jmatile_n2()`
    - 降水ナウキャストの配信時刻

##### Retrieve the Feed details

- `#request_xml(url)`

```ruby
jma_client = JMA::Client.new.tap do |config|
	config.response_type = :deserialize
end
jma_client.request_xml('http://www.data.jma.go.jp/developer/xml/data/20211229170106_0_VFVO52_400000.xml')
```


## Development

### Test

```bash
rspec spec
```

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/routeflagsinc/jma. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://bitbucket.org/routeflagsinc/jma/src/master/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Inspiration
- [octokit/octokit.rb: Ruby toolkit for the GitHub API](https://github.com/octokit/octokit.rb)

## Copyrights
*1 出典：気象庁ホームページ [気象庁防災情報XML - DATA GO JP](https://www.data.go.jp/data/dataset/mlit_20170902_0034)

## References
- [気象庁ホームページについて](https://www.jma.go.jp/jma/kishou/info/coment.html)
- [気象庁防災情報XMLフォーマット　情報提供ページ](http://xml.kishou.go.jp/)
- [気象庁 | 気象庁防災情報XMLフォーマット形式電文（PULL型）](http://xml.kishou.go.jp/xmlpull.html)
- [気象庁防災情報XML - DATA GO JP](https://www.data.go.jp/data/dataset/mlit_20170902_0034)
- [【Colab / Python】気象庁API - 気象データの収集](https://qiita.com/T_Ryota/items/ef96d6575404a0fd46dd)
- [気象庁防災XMLのPull型提供（Atom Feed）の品質 - Qiita](https://qiita.com/e_toyoda/items/2942903d406ed96c6bed)
- [気象庁防災XMLのPull型提供（Atom Feed）について - Qiita](https://qiita.com/e_toyoda/items/185c1dea055e230918b9)
- [気象庁ホームページ防災気象情報のURL構造 - Qiita](https://qiita.com/e_toyoda/items/7a293313a725c4d306c0)
- [ピュアRubyでAtomも対応したRSS Parserを作ってみたMEMO ](https://madogiwa0124.hatenablog.com/entry/2019/03/26/214228)
- [現在出ている気象警報・注意報を取得する](https://qiita.com/conqueror/items/f72cfdedd01149c35313)
